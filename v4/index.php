<?php
session_start();

function getRealIp() {
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {  //check ip from share internet
		$ip=$_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  //to check ip is pass from proxy
		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip=$_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}

function writeLog($where) {

	$ip = getRealIp(); // Get the IP from superglobal
	$host = gethostbyaddr($ip);    // Try to locate the host of the attack
	$date = date("d M Y");

	// create a logging message with php heredoc syntax
	$logging = <<<LOG
			\n
			<< Start of Message >>
			There was a hacking attempt on your form. \n
			Date of Attack: {$date}
			IP-Adress: {$ip} \n
			Host of Attacker: {$host}
			Point of Attack: {$where}
			<< End of Message >>
LOG;
// Awkward but LOG must be flush left

	// open log file
	if($handle = fopen('hacklog.log', 'a')) {

		fputs($handle, $logging);  // write the Data to file
		fclose($handle);           // close the file

	} else {  // if first method is not working, for example because of wrong file permissions, email the data

		$to = 'david.cuello@r2da.com';
		$subject = 'HACK ATTEMPT';
		$header = 'From: david.cuello@r2da.com';
		if (mail($to, $subject, $logging, $header)) {
			echo "Sent notice to admin.";
		}

	}
}

function verifyFormToken($form) {

	// check if a session is started and a token is transmitted, if not return an error
	if(!isset($_SESSION[$form.'_token'])) {
		return false;
	}

	// check if the form is sent with token in it
	if(!isset($_POST['token'])) {
		return false;
	}

	// compare the tokens against each other if they are still the same
	if ($_SESSION[$form.'_token'] !== $_POST['token']) {
		return false;
	}

	return true;
}

function generateFormToken($form) {

	// generate a token from an unique value, took from microtime, you can also use salt-values, other crypting methods...
	$token = md5(uniqid(microtime(), true));

	// Write the generated token to the session variable to check it against the hidden field when the form is sent
	$_SESSION[$form.'_token'] = $token;

	return $token;
}

// VERIFY LEGITIMACY OF TOKEN
if (verifyFormToken('form1')) {

	// CHECK TO SEE IF THIS IS A MAIL POST
	if (isset($_POST['e'])) {

		// Building a whitelist array with keys which will send through the form, no others would be accepted later on
		$whitelist = array('token','e','t','n','m','mo');

		// Building an array with the $_POST-superglobal
		foreach ($_POST as $key=>$item) {

			// Check if the value $key (fieldname from $_POST) can be found in the whitelisting array, if not, die with a short message to the hacker
			if (!in_array($key, $whitelist)) {

				writeLog('Unknown form fields');
				die("Hack-Attempt detected. Please use only the fields in the form");

			}
		}









		// PREPARE THE BODY OF THE MESSAGE

		$message = '<html><body>';
		$message .= '<img src="http://r2da.com/images/brand.png" alt="R2DA.com" />';
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td>" . strip_tags($_POST['n']) . "</td></tr>";
		$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($_POST['e']) . "</td></tr>";
		$message .= "<tr><td><strong>Movil:</strong> </td><td>" . strip_tags($_POST['mo']) . "</td></tr>";
		$message .= "<tr><td><strong>Asunto:</strong> </td><td>" . strip_tags($_POST['t']) . "</td></tr>";
		$message .= "<tr><td><strong>Mensaje:</strong> </td><td>" . strip_tags($_POST['m']) . "</td></tr>";
		$message .= "</table>";
		$message .= "</body></html>";




		//  MAKE SURE THE "FROM" EMAIL ADDRESS DOESN'T HAVE ANY NASTY STUFF IN IT

		$pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i";
		if (preg_match($pattern, trim(strip_tags($_POST['e'])))) {
			$cleanedFrom = trim(strip_tags($_POST['e']));
		} else {
			return "The email address you entered was invalid. Please try again!";
		}




		//   CHANGE THE BELOW VARIABLES TO YOUR NEEDS

		$to = 'david.cuello@r2da.com';

		$subject = 'Mensaje desde la web de R2DA';

		$headers = "From: " . $cleanedFrom . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		if (mail($to, $subject, $message, $headers)) {
			echo 'Your message has been sent.';
		} else {
			echo 'There was a problem sending the email.';
		}

		// DON'T BOTHER CONTINUING TO THE HTML...
		die();

	}
} else {

	if (!isset($_SESSION[$form.'_token'])) {

	} else {
		echo "Hack-Attempt detected. Got ya!.";
		writeLog('Formtoken');
	}

}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="Theme Region">
		<meta name="description" content="">

		<title>R2DA</title>

		<!-- CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/slick.css">
		<link rel="stylesheet" href="css/sidr.css">
		<link rel="stylesheet" href="css/magnific-popup.css">
		<link rel="stylesheet" href="css/cubeportfolio.min.css">
		<link rel="stylesheet" href="css/animate.css">
		<link rel="stylesheet" href="css/main.css">
			<link id="preset" rel="stylesheet" href="css/presets/preset3.css">
		<link rel="stylesheet" href="css/responsive.css">

		<!-- font -->
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700,800' rel='stylesheet' type='text/css'>

		<!-- icons -->
		<link rel="icon" href="images/ico/favicon.ico">
		<link rel="apple-touch-icon" sizes="144x144" href="../images/ico/apple-touch-icon-144.png">
		<link rel="apple-touch-icon" sizes="114x114" href="../images/ico/apple-touch-icon-114.png">
		<link rel="apple-touch-icon" sizes="72x72" href="../images/ico/apple-touch-icon-72.png">
		<link rel="apple-touch-icon" sizes="57x57" href="../images/ico/apple-touch-icon-57.png">
		<!-- icons -->

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<?php
	// generate a new token for the $_SESSION superglobal and put them in a hidden field
	$newToken = generateFormToken('form1');
	?>
	<body class="tr-homepage homepage-2">
		<div id="preloader">
			<div id="status">&nbsp;</div>
		</div>
		<div class="tr-main-wrapper">
			<nav class="navbar navbar-custom navbar-fixed-top">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"> <img src="images/brand.png" class="img-responsive" alt=""> </a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="#tr-home">Home</a>

							</li>
							<li><a href="#tr-service">Servicios</a></li>
							<li><a href="#tr-portfolio">Portafolio</a></li>
							<li><a href="#tr-about">Clientes</a></li>
							<li><a href="#tr-contact">Contacto</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
			<div id="tr-home" class="tr-home-slider">
				<div id="home-carousel" class="carousel slide" data-ride="carousel">

					<div class="carousel-inner" role="listbox">
						<div class="item active" style=" background-image: url(images/slider/slider4.jpg)">
							<div class="overlay"></div>
							<div class="slide-content">
								<div class="tr-middle">
									<div class="container">
										<div class="row">
											<div class="col-sm-8 col-sm-offset-2">
												<div class="home-content text-center">
													<div class="tr-logo"  data-animation="animated fadeInDown">
														<a href="index.html"><img class="img-responsive" src="images/logo.png" alt="Logo"></a>
													</div>
													<h1 data-animation="animated flipInX">la fuerza de la innovación </h1>
													<a href="#tr-contact" class="btn btn-primary" data-animation="animated fadeInUp">Quiero innovar! <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
<!--
													<div class="video" data-animation="animated fadeInUp">
														<a class="video-link" href="https://www.youtube.com/watch?v=AI3pORAXU4g"><i class="fa fa-youtube-play"></i><span>Watch Video</span></a>
													</div> video
-->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><!-- /.item -->

						<!--<div class="item item-3" style=" background-image: url(images/slider/slider1.jpg)">
							<div class="overlay"></div>
							<div class="slide-content">
								<div class="tr-middle">
									<div class="tr-feed">
										<ul class="pull-right list-inline">
											<li>Find Us</li>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
										</ul>
									</div>
									<div class="container">
										<div class="row">
											<div class="col-sm-6">
												<div class="home-content">
													<h1 data-animation="animated fadeInLeft">Hello! I am <span class="color">Jimmy</span></h1>
													<a href="#" class="btn btn-primary" data-animation="animated fadeInRight">Learn More About Us <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>--><!-- /.item -->


					</div><!-- /.carousel-inner -->
				</div><!-- /#home-carousel -->
			</div><!-- /.tr-home-slider -->

			<div id="tr-service" class="tr-service image-bg section-padding">
				<div class="container">
					<div class="section-info">
						<h2>Somos un <span>laboratorio</span> de ideas</h2>
						<p>Construimos soluciones a medida para empresas grandes y pequeñas usando la tecnología.</p>
					</div>
					<div class="service-content">
						<div class="row">
							<div class="col-sm-3">
								<div class="service icon-1">
									<img class="img-responsive center-block" src="images/service/1.png" alt="Service Image">
									<h3>Diseño centrado en el usuario </h3>
									<p>Un cliente satisfecho es la mejor validación </p>

								</div>
							</div>

							<div class="col-sm-3">
								<div class="service icon-2">
									<img class="img-responsive center-block" src="images/service/2.png" alt="Service Image">
									<h3>Desarrollo a medida</h3>
									<p>Lo que necesitas, sin adicionales ni preservantes</p>

								</div>
							</div>

							<div class="col-sm-3">
								<div class="service icon-3">
									<img class="img-responsive center-block" src="images/service/3.png" alt="Service Image">
									<h3> Multi Plataformas</h3>
									<p>Aplicación movil nativa, hibrída o responsiva?</p>

								</div>
							</div>

							<div class="col-sm-3">
								<div class="service icon-4">
									<img class="img-responsive center-block" src="images/service/4.png" alt="Service Image">
									<h3> El ingrediente secreto</h3>
									<p>Hacer lo que nos gusta, es lo que nos mueve</p>

								</div>
							</div>
						</div>
					</div>
				</div><!-- container -->
			</div><!-- our service -->

			<div id="tr-portfolio" class="tr-portfolio">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 ">
							<h2>Nuestros trabajos</h2>
						</div>
					</div>
				</div>

				<div class="portfolio-slider">
					<div class="portfolio-item">
						<img class="img-responsive" src="images/portfolio/wsp_express.jpg" alt="Portfolio Image">
						<div class="portfolio-overlay">
							<div class="portfolio-info">
								<h2>WSP</h2>
								<p>Diseño y desarrollo de aplicación nativa para validación biometrica.</p>
								<a href="wsp-details.html" class="btn btn-default">ver más <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
					<div class="portfolio-item">
						<img class="img-responsive" src="images/portfolio/ayudantee.jpg" alt="Portfolio Image">
						<div class="portfolio-overlay">
							<div class="portfolio-info">
								<h2>Ayudantee</h2>
								<p>Plataforma en linea de ayudantias y profesores</p>
								<a href="ayudantee-details.html" class="btn btn-default">ver más <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
					<div class="portfolio-item">
						<img class="img-responsive" src="images/portfolio/caaapital.jpg" alt="Portfolio Image">
						<div class="portfolio-overlay">
							<div class="portfolio-info">
								<h2>Caaapital</h2>
								<p>Data Room para almacenar toda la documentación técnica, legal y comercial.</p>
								<a href="caaapital-details.html" class="btn btn-default">ver más <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
					<div class="portfolio-item">
						<img class="img-responsive" src="images/portfolio/icar.jpg" alt="Portfolio Image">
						<div class="portfolio-overlay">
							<div class="portfolio-info">
								<h2>I-Car</h2>
								<p>Inteligencia Artificial para registro de matriculas de vehiculos.</p>
								<a href="icar-details.html" class="btn btn-default">ver más <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>


				</div><!-- portfolio slider -->
<!-- playground
				<div class="container">
					<div class="row text-center">
						<div class="col-xs-12 ">


							<h1 data-animation="animated flipInX">Quieres ver nuestros inventos y juguetes? </h1>
							<a href="#" class="btn btn-primary" data-animation="animated fadeInUp">pasa a nuestro patio de juegos <i class="fa fa-chevron-right" aria-hidden="true"></i></a>

						</div>
					</div>
				</div>
-->
			</div><!-- tr-portfolio -->

			<div id="tr-about" class="tr-about image-bg section-padding  ">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="tr-clients ">
								<h2>Nuestros Clientes</h2>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><img src="images/clients/acepta.png" alt="" class="img-responsive center-block"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><img src="images/clients/ayudantee.png" alt="" class="img-responsive center-block"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><img src="images/clients/caaapital.png" alt="" class="img-responsive center-block"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><img src="images/clients/digital_bank.png" alt="" class="img-responsive center-block"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><img src="images/clients/eneroom.png" alt="" class="img-responsive center-block"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><img src="images/clients/grupo_componente.png" alt="" class="img-responsive center-block"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><img src="images/clients/i-car.png" alt="" class="img-responsive center-block"></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><img src="images/clients/wsp.png" alt="" class="img-responsive center-block"></div>
					</div><!-- row -->
				</div><!-- container -->
			</div><!-- about us -->


			<div id="tr-contact" class="tr-contact-section contact-1 section-padding">
				<div class="container text-center">
					<div class="section-title">
						<h1>Contáctanos</h1>
					</div>
					<address>
						<p>Dr. Carlos Charlin 101, Piso 1 - Fintech Lab
						Providencia, Santiago - Chile <br>Email: <a href="#">hola@r2da.com</a></p>
					</address>

					<div class="col-xs-12 col-lg-8">
					<form class="contact-form">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" id="iname" class="form-control" required="required" placeholder="Nombre">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input type="email" id="imail" class="form-control" required="required" placeholder="Email">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input type="number" id="imobile" class="form-control" required="required" placeholder="Móvil">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" id="ititle" class="form-control" required="required" placeholder="Asunto">
										<input type="hidden" id="token" value="<?php echo $newToken; ?>">

									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<textarea name="message" id="imessage" required="required" class="form-control" rows="7" placeholder="Ingresar mensaje"></textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div id="btnS" class="btn btn-primary">Enviar <i class="fa fa-chevron-right" aria-hidden="true"></i></div>
							</div>

					</form><!-- /.contact form -->
					</div>
					<div class="col-lg-4 col-xs-12">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3329.914894804479!2d-70.62285418480126!3d-33.425463080781086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662cf621946a033%3A0x463771d00c67f133!2sDr+Carlos+Charlin%2C+Providencia%2C+Regi%C3%B3n+Metropolitana%2C+Chile!5e0!3m2!1ses!2s!4v1495212563471" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div><!-- /.container -->
			</div><!-- /.contact us -->
		</div><!-- tr home -->

		<div class="footer-section">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">


							<div class="copyright">
								<p>&copy; 2017 R2DA. </p>
							</div>

					</div>



				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.footer-section -->


		<!-- JS -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAkpTXjbIvi8swGGEb_y_HuLXY5d9WnRNQ&sensor=false"></script>
		<script src="js/slick.min.js"></script>
		<script src="js/jquery.sidr.min.js"></script>
		<script src="js/magnific-popup.min.js"></script>
		<script src="js/cubeportfolio.min.js"></script>
		<script src="js/jquery.nav.js"></script>
		<script src="js/theia-sticky-sidebar.js"></script>
		<script src="js/main.js"></script>
		<script src="js/app.js"></script>
	</body>
</html>
