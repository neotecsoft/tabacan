<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Desarrollamos tu idea. MARKETING/ SOFTWARE/ DESIGN/">
    <meta name="author" content="r2da.com">
    <meta name="og:image" content="http://r2da.com/img/share.jpg">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>R2DA</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="css/animate.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/creative.css" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">
<div class="bck">

</div>
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand page-scroll" href="#page-top"><img src="img/logo.png" class="logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#about">Acerca de</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">Servicios</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">Portafolio</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contacto</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
    <div class="wrapper">
    </div>


        <div class="header-content">
            <div class="header-content-inner">
                <h1>Soluciones Web y Móviles Para Los Desafios Del Mañana</h1>
                <hr>
                <p class="subtitle">¿Necesitas ayuda?</p>
                <br>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Conócenos más</a>
            </div>
        </div>
    </header>

    <section class="bg-dark" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h1 class="section-heading">¿Qué Hacemos?</h1>
                    <hr class="light">
                    <p class="text-faded">Construimos software a medida para empresas grandes y pequeñas. Mediante la aplicación de los principios de diseño moderno, en conjunto con las últimas tecnologías de nube, móviles y de escritorio, creamos soluciones que permiten simplificar y acelerar los procesos e impulsar los ingresos de su negocio.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12  text-center">
                    <h1 class="section-heading">Nuestros Servicios</h1>
                    <br>
                    <hr>
                    <br>

                    <div class="col-lg-6  text-center">

                    <h2 class="section-heading">Construimos Software a Medida</h2>
                    <hr class="light">
                    <p class="text-wite">Diseñamos y construimos software a medida que resuelven sus problemas más complejos, se adaptan perfectamente a sus procesos, y producen resultados medibles para su negocio. Desarrollamos plataformas web como móviles.
                    </p>
                </div>

                <div class="col-lg-6 text-center">
                    <h2 class="section-heading">Administramos sus Software</h2>
                    <hr class="light">
                    <p class="text-wite">Después del desarrollo, mantendremos las cosas funcionando sin problemas para que pueda centrarse en su negocio. Vamos a configurar un entorno de producción para sus aplicaciones y supervisar la seguridad, actualizaciones de servidor, monitorización del rendimiento, copias de seguridad y el mantenimiento regular de características y cambios.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="no-padding" id="portfolio">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="img/portfolio/talkschat.jpg" class="img-responsive" alt="Talks.Chat">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    WebRTC - Comunicacion
                                </div>
                                <div class="project-name">
                                    Talks.chat
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="img/portfolio/ayudantee.jpg" class="img-responsive" alt="Ayudantee">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Ayudante universitarios - Pizarra - WebRTC
                                </div>
                                <div class="project-name">
                                    Ayudantee.com
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="img/portfolio/casesurfer.jpg" class="img-responsive" alt="CaseSurfer">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    App iOs/Android
                                </div>
                                <div class="project-name">
                                    CaseSurfer
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="img/portfolio/otadmin.jpg" class="img-responsive" alt="OT - Admin">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Web - Gestion
                                </div>
                                <div class="project-name">
                                    OT Admin <small>Ordenes de Trabajo</small>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                <a href="#" class="portfolio-box">
                    <img src="img/portfolio/caaapital.jpg" class="img-responsive" alt="Caaapital">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Web - Seguridad - Data Room
                            </div>
                            <div class="project-name">
                                Virtual Data Room Caaapital
                            </div>
                        </div>
                    </div>
                </a>
            </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="img/portfolio/doonde.jpg" class="img-responsive" alt="DoondeCL">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Web - Geolocalización
                                </div>
                                <div class="project-name">
                                    Doonde.com
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </section>



    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Vamos a estar en contacto!</h2>
                    <hr class="primary">
                    <p>Listo para iniciar su próximo proyecto con nosotros? ¡Eso es genial! Llámenos o envie un correo electrónico y nos pondremos en contacto con usted tan pronto como sea posible!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x wow bounceIn"></i>
                    <p><a href="tel:+56972518446">+56 9 72518446</a></p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
                    <p><a href="mailto:contacto@r2da.com">contacto@r2da.com</a></p>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/wow.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/creative.js"></script>

</body>

</html>
